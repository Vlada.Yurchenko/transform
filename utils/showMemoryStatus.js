function memoryUsage() {
  const used = process.memoryUsage().heapUsed / 1024 / 1024;
  let message = `Your app uses ${used.toFixed(2)}MB`;
  if(used > 20) {
    message = 'WARN!!! WARN!!! WARN!!! ' + message;
  }

  console.log(message);
}

const intervalId = setInterval(memoryUsage, 1000)

module.exports = () => setTimeout(() => clearInterval(intervalId), 3000);

const fs = require('fs');
const mStatusEnd = require('./utils/showMemoryStatus');
const TransformStr = require('./transform');

const transform = new TransformStr();

const readable = fs.createReadStream('./data.txt');
const writable = fs.createWriteStream('result.txt');

console.log('start');
console.time('transform');
const result = readable.pipe(transform).pipe(writable);

result.on('close', () => {
  console.log('Done!');
  console.timeEnd('transform');
  mStatusEnd();
});

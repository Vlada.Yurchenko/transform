## get started
```npm i```

```npm run build``` - create `data.txt`(incomming data)

use arguments, like
```
npm run build 1
npm run build 10
npm run build 0.1
```
to genetare smaller files(only for testing), by default - 100Mb

# Task
- You have a lot of incoming data like:
```
 _  _  _  _  _     _  _    _
|_||_|| ||_ |_ |_| _| _| || |
 _||_|  ||_| _|  | _||_  ||_|
```
- You need parse it to:
```
987654321
```
- if it is a not valid number, replace it to `?`
- in one line always 10 numbers, one number always contains 9 chars(3*3)

# Steps:

- Write your stream in `transform.js`

- run ```npm start```

- See result in `result.txt`

### Rememder about SOLID, DRY, KISS ...
